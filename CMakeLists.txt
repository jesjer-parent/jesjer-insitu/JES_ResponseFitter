################################################################################
# Package: JES_ResponseFitter
################################################################################

# Declare the package name:
atlas_subdir( JES_ResponseFitter )

# Find the needed externals:
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO Graf Gpad )

# build a dictionary for the library
atlas_add_root_dictionary ( JES_ResponseFitterLib JES_ResponseFitterDictSource
                            ROOT_HEADERS JES_ResponseFitter/*.h Root/LinkDef.h
                            EXTERNAL_PACKAGES ROOT
)

# build a shared library
atlas_add_library( JES_ResponseFitterLib JES_ResponseFitter/*.h Root/*.cxx ${JES_ResponseFitterDictSource}
                   PUBLIC_HEADERS JES_ResponseFitter
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                   LINK_LIBRARIES ${ROOT_LIBRARIES}
)

# add executables
atlas_add_executable( DrawTruthResponse
   util/DrawTruthResponse.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} JES_ResponseFitterLib )

atlas_add_executable( FitZjetDB
   util/FitZjetDB.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} JES_ResponseFitterLib )

