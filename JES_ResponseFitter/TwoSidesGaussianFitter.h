#pragma once
#include <vector>
#include <numeric>
#include "Rtypes.h"
#include "TH1.h"
#include "TF1.h"

/// <summary>
/// A class to perform two sides Gaussian fit.
/// For more information on the method, see: https://indico.cern.ch/event/711895/contributions/3125275/
/// 
/// Example usage:
/// auto m_R_vs_E_2SideFit_Graph = std::make_shared<TGraphErrors>();
/// m_R_vs_E_2SideFit_Graph->SetNameTitle("R_vs_E_2SideFit", "R_vs_E_2SideFit");
/// TwoSidesGaussianFitter tsgf;
/// tsgf.Fit(*inputHist);
/// auto twoSideFitMeanStdevPair = tsgf.getCombinedMeanStdev();
/// m_R_vs_E_2SideFit_Graph->SetPoint(pointN, avg, twoSideFitMeanStdevPair.first);
/// m_R_vs_E_2SideFit_Graph->SetPointError(pointN, avgErr, twoSideFitMeanStdevPair.second);
/// </summary>
class TwoSidesGaussianFitter
{
public:
	TwoSidesGaussianFitter();
	~TwoSidesGaussianFitter();
	void clear();

	enum class RangeMode
	{
		AllCombinationInSmoothMode,
		BestRChi2InSmoothMode,
		MaxBin,
		UseAllBinInSmoothModeForBoth,
	};
	RangeMode m_rangeMode = RangeMode::UseAllBinInSmoothModeForBoth;

	enum class WeightMode
	{
		NBins,
		BinContents,
		InverseRChi2,
	};
	WeightMode m_weightMode = WeightMode::BinContents;

	enum class DistributionRangeMode
	{
		All,
		IgnoreEmptyBins,
		IgnoreSideBinContent,
	};
	double m_fractionOfSideBinContentToIgnore = 0.02;
	DistributionRangeMode m_distributionRangeMode = DistributionRangeMode::IgnoreSideBinContent;

	bool m_addCenterFit = true;

	std::vector<double> m_lFitMeans, m_rFitMeans;
	std::vector<double> m_lFitWeights, m_rFitWeights;

	double m_lMean, m_rMean, m_cMean;
	double m_lStdev, m_rStdev, m_cStdev;
	double m_lWeight, m_rWeight, m_cWeight;

	void fit(TH1& hist);
	std::pair<double, double> getCombinedMeanStdev();

	struct SmoothModeResult
	{
		std::vector<Int_t> ModeBins;
		Int_t MaxBin;
	};

	/// <summary>
	/// Find bins that include mode and those that have error range compatible with it. Adapt from uniModalSmoothMax in Utilities
	/// The method assumes that h is unimodal (it has only 1 maximum).
	///  * find the bin having  the absolute maximum content (call the bin center mode_0, the max value content_0)
	///  * find all bins i with value statiscally compatible with max0 (that is for wich content_0-content_0_err< content_i < content_0 ), call their center mode_i 
	/// This procedures is performed ignoring all "outliers" bins where a bin is an outlier if "content_err/content > outlierF"
	/// </summary>
	/// <param name="h">The histogram</param>
	/// <param name="outlierF">Outliner criteria</param>
	/// <param name="startBin">bin to start the search</param>
	/// <returns></returns>
	static SmoothModeResult smoothModeBins(const TH1& h, float outlierF = 0.5, int startBin = 1);

	//https://stackoverflow.com/a/7616783
	template<typename T> static std::pair<double, double> computeMeanAndStdev(std::vector<T> v)
	{
		double sum = std::accumulate(std::begin(v), std::end(v), 0.0);
		double mean = sum / v.size();
		std::vector<T> diff(v.size());
		std::transform(v.begin(), v.end(), diff.begin(), [mean](double x) { return x - mean; });
		double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
		return std::make_pair(mean, std::sqrt(sq_sum / (v.size() - 1)));
	}

	/// <summary>
	/// Compute weighted mean and the approximated �standard error of the weighted mean�
	/// https://doi.org/10.1016/1352-2310(94)00210-C
	/// </summary>
	/// <param name="v">std::vector of values</param>
	/// <param name="w">std::vector of weights</param>
	/// <returns>std::pair<mean, stdev></returns>
	template<typename T> static std::pair<double, double> computeWeightedMeanAndStdev(std::vector<T> v, std::vector<T> w)
	{
		double sum = std::inner_product(v.begin(), v.end(), w.begin(), 0.0);
		double sumw = std::accumulate(std::begin(w), std::end(w), 0.0);

		double mean = sum / sumw;

		std::vector<double> diff2(v.size());
		std::transform(v.begin(), v.end(), diff2.begin(), [mean](double x) { return std::pow(x - mean, 2); });
		std::vector<double> w2(v.size());
		std::transform(w.begin(), w.end(), w2.begin(), [](double w) { return std::pow(w, 2); });
		//double sumw2 = std::inner_product(w.begin(), w.end(), diff2.begin(), 0.0);

		//double diffsqw = std::inner_product(diff2.begin(), diff2.end(), w.begin(), 0.0);
		double diffsqwsq = std::inner_product(diff2.begin(), diff2.end(), w2.begin(), 0.0);
		//return std::make_pair(mean, std::sqrt(diffsqw / (sumw - (sumw2/sumw))));
		double n = v.size();
		return std::make_pair(mean, std::sqrt((n / ((n - 1)*std::pow(sumw, 2))) * diffsqwsq));
	}

	std::pair<size_t, size_t> getNumberOfIgnoreSideBins(const TH1& hist);

private:
	std::string baseFitOptions = "WLRQ0S";
};
