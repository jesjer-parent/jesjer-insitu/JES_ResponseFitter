#include "JES_ResponseFitter/TwoSidesGaussianFitter.h"
#include "TFitResult.h"

TwoSidesGaussianFitter::TwoSidesGaussianFitter()
{

}


TwoSidesGaussianFitter::~TwoSidesGaussianFitter()
{
}

void TwoSidesGaussianFitter::clear()
{
	m_lFitMeans.clear();
	m_rFitMeans.clear();
	m_lFitWeights.clear();
	m_rFitWeights.clear();
}

void TwoSidesGaussianFitter::fit(TH1 & hist)
{
	clear();

	//Find the mode and bins that consistant with them
	auto smoothModeResult = smoothModeBins(hist);

	//Side bins on the side to ignore
	const auto lrNIgnoreBinPair = getNumberOfIgnoreSideBins(hist);
	const auto& lNIgnoreBin = lrNIgnoreBinPair.first;
	const auto& rNIgnoreBin = lrNIgnoreBinPair.second;
	Int_t lEdgeBin = 0, rEdgeBin = hist.GetNbinsX();
	lEdgeBin += lNIgnoreBin;
	rEdgeBin -= rNIgnoreBin;



	std::vector<std::shared_ptr<TF1>> lFunctions, rFunctions, cFunctions;

	//Add all possible ranges. Can be any bin in the smooth mode range from the same side of the function (i.e. Left-Function can use #bin < #max bin)
	if (m_rangeMode == RangeMode::AllCombinationInSmoothMode || m_rangeMode == RangeMode::BestRChi2InSmoothMode)
	{
		for (auto&& binIndex : smoothModeResult.ModeBins)
		{
			if (binIndex >= smoothModeResult.MaxBin)
			{
				//For right-side function, the ranges are the upper edge of #bin in the mode list to the last bin
				auto rFunc = std::make_shared<TF1>(TString(hist.GetName()) + "_rfunc_" + std::to_string(binIndex), "gaus", hist.GetBinLowEdge(binIndex), hist.GetBinLowEdge(rEdgeBin + 1));
				m_rFitWeights.push_back(hist.Integral(hist.GetBinLowEdge(binIndex), hist.GetBinLowEdge(rEdgeBin + 1)));
				rFunctions.push_back(std::move(rFunc));
			}

			if (binIndex <= smoothModeResult.MaxBin)
			{
				//std::cout << binIndex << " " << hist.GetBinLowEdge(binIndex) << " " << smoothModeResult.MaxBin << std::endl; std::cin.get();
				//For left-side function, the ranges are the first bin to the lower edge of #bin in the mode list
				auto lFunc = std::make_shared<TF1>(TString(hist.GetName()) + "_lfunc_" + std::to_string(binIndex), "gaus", hist.GetBinLowEdge(lEdgeBin), hist.GetBinLowEdge(binIndex));
				//Use (content) integral as the weight
				m_lFitWeights.push_back(hist.Integral(hist.GetBinLowEdge(lEdgeBin), hist.GetBinLowEdge(binIndex)));
				lFunctions.push_back(std::move(lFunc));
			}
		}
	}

	const auto firstModeBinIndex = smoothModeResult.ModeBins.front();
	const auto lastModeBinIndex = smoothModeResult.ModeBins.back();
	std::shared_ptr<TF1> lFunc_extra, rFunc_extra;
	if (m_rangeMode == RangeMode::MaxBin)
	{
		lFunc_extra = std::make_shared<TF1>(TString(hist.GetName()) + "_lfunc_" + std::to_string(smoothModeResult.MaxBin), "gaus", hist.GetBinLowEdge(lEdgeBin), hist.GetBinLowEdge(smoothModeResult.MaxBin));
		m_lFitWeights.push_back(hist.Integral(hist.GetBinLowEdge(lEdgeBin), hist.GetBinLowEdge(smoothModeResult.MaxBin)));
		rFunc_extra = std::make_shared<TF1>(TString(hist.GetName()) + "_rfunc_" + std::to_string(firstModeBinIndex), "gaus", hist.GetBinLowEdge(smoothModeResult.MaxBin), hist.GetBinLowEdge(rEdgeBin + 1));
		m_rFitWeights.push_back(hist.Integral(hist.GetBinLowEdge(smoothModeResult.MaxBin), hist.GetBinLowEdge(rEdgeBin + 1)));
	}
	else
	{
		//Functions that include all ModeBins, one each for both side
		lFunc_extra = std::make_shared<TF1>(TString(hist.GetName()) + "_lfunc_" + std::to_string(lastModeBinIndex), "gaus", hist.GetBinLowEdge(lEdgeBin), hist.GetBinLowEdge(lastModeBinIndex + 1));
		m_lFitWeights.push_back(hist.Integral(hist.GetBinLowEdge(lEdgeBin), hist.GetBinLowEdge(lastModeBinIndex + 1)));
		rFunc_extra = std::make_shared<TF1>(TString(hist.GetName()) + "_rfunc_" + std::to_string(firstModeBinIndex), "gaus", hist.GetBinLowEdge(firstModeBinIndex), hist.GetBinLowEdge(rEdgeBin + 1));
		m_rFitWeights.push_back(hist.Integral(hist.GetBinLowEdge(firstModeBinIndex), hist.GetBinLowEdge(rEdgeBin + 1)));
	}
	lFunctions.push_back(std::move(lFunc_extra));
	rFunctions.push_back(std::move(rFunc_extra));

	double minRChi2 = std::numeric_limits<double>::infinity();
	std::shared_ptr<TF1> minRChi2LFunc, minRChi2RFunc;
	std::string fitOptions = baseFitOptions;
	if (m_rangeMode == RangeMode::BestRChi2InSmoothMode)
	{
		fitOptions += "N";
	}
	else
	{
		fitOptions += "+";
	}

	for (auto&& f : lFunctions)
	{
		f->SetParameters(hist.GetMaximum(), hist.GetMean(), hist.GetRMS());
		//f->SetParLimits(2, 0., 1.0);
		f->SetLineColor(kGreen);

		TFitResultPtr fitResult = hist.Fit(f.get(), fitOptions.c_str());

		if (m_rangeMode == RangeMode::BestRChi2InSmoothMode)
		{
			double rChi2 = f->GetChisquare() / f->GetNDF();
			if (rChi2 <= minRChi2)
			{
				minRChi2LFunc = f;
				minRChi2 = rChi2;
			}
		}
		else
		{
			m_lFitMeans.push_back(f->GetParameter(1));
		}
	}
	if (m_rangeMode == RangeMode::BestRChi2InSmoothMode)
	{
		hist.Fit(minRChi2LFunc.get(), (baseFitOptions + "+").c_str());
		m_lFitMeans.push_back(minRChi2LFunc->GetParameter(1));
		minRChi2 = std::numeric_limits<double>::infinity();
	}

	for (auto&& f : rFunctions)
	{
		f->SetParameters(hist.GetMaximum(), hist.GetMean(), hist.GetRMS());
		//f->SetParLimits(2, 0., 1.0);
		f->SetLineColor(kBlue);

		TFitResultPtr fitResult = hist.Fit(f.get(), fitOptions.c_str());

		if (m_rangeMode == RangeMode::BestRChi2InSmoothMode)
		{
			double rChi2 = f->GetChisquare() / f->GetNDF();
			if (rChi2 < minRChi2)
			{
				minRChi2RFunc = f;
				minRChi2 = rChi2;
			}
		}
		else
		{
			m_rFitMeans.push_back(f->GetParameter(1));
		}
	}
	if (m_rangeMode == RangeMode::BestRChi2InSmoothMode)
	{
		hist.Fit(minRChi2RFunc.get(), (baseFitOptions + "+").c_str());
		m_rFitMeans.push_back(minRChi2RFunc->GetParameter(1));
	}

	//Center fit
	Int_t cLowBin = 0, cHighBin = 0;
	if (m_addCenterFit)
	{
		//Take half the bins to each side from the max bin (round down for left side, round up for right side)
		cLowBin = smoothModeResult.MaxBin - ((smoothModeResult.MaxBin - lNIgnoreBin + 1) / 2);
		cHighBin = ((rEdgeBin - smoothModeResult.MaxBin) / 2) + smoothModeResult.MaxBin + 1;
		auto cFunc = std::make_shared<TF1>(TString(hist.GetName()) + "_cfunc", "gaus", hist.GetBinLowEdge(cLowBin), hist.GetBinLowEdge(cHighBin));
		//std::cout << hist.GetBinLowEdge(cLowBin) << " " << hist.GetBinLowEdge(cHighBin) << std::endl; std::cin.get();
		cFunc->SetParameters(hist.GetMaximum(), hist.GetMean(), hist.GetRMS());
		cFunc->SetLineColor(kCyan);
		TFitResultPtr fitResult = hist.Fit(cFunc.get(), (baseFitOptions + "+").c_str());
		cFunctions.push_back(cFunc);
	}

	//Take the last of Left-function and the first of right function, which would be the ones use MaxBin as edge
	auto lFunc = lFunctions.back();
	auto rFunc = rFunctions.front();
	auto cFunc = cFunctions.front();
	switch (m_weightMode)
	{
	case TwoSidesGaussianFitter::WeightMode::NBins:
	{
		throw std::logic_error("Not yet implemented");
	}
	break;
	case TwoSidesGaussianFitter::WeightMode::BinContents:
	{
		//Use the bin content on the left and right of the mode bin as weight. The mode bin is divide half and half
		auto modeBinContentHalf = hist.GetBinContent(smoothModeResult.MaxBin) / 2;
		m_lWeight = hist.Integral(lEdgeBin, smoothModeResult.MaxBin - 1) + modeBinContentHalf;
		m_rWeight = hist.Integral(smoothModeResult.MaxBin + 1, rEdgeBin) + modeBinContentHalf;
		if (m_addCenterFit)
		{
			m_cWeight = hist.Integral(cLowBin, cHighBin);
		}
	}
	break;
	case TwoSidesGaussianFitter::WeightMode::InverseRChi2:
	{
		m_lWeight = lFunc->GetNDF() / lFunc->GetChisquare();
		m_rWeight = rFunc->GetNDF() / rFunc->GetChisquare();
		if (m_addCenterFit)
		{
			m_cWeight = cFunc->GetNDF() / cFunc->GetChisquare();
		}
	}
	break;
	default:
		break;
	}

	auto lMeanStdevPair = TwoSidesGaussianFitter::computeMeanAndStdev(m_lFitMeans);
	auto rMeanStdevPair = TwoSidesGaussianFitter::computeMeanAndStdev(m_rFitMeans);
	m_lMean = lMeanStdevPair.first;
	m_lStdev = lMeanStdevPair.second;
	m_rMean = rMeanStdevPair.first;
	m_rStdev = rMeanStdevPair.second;
	if (m_addCenterFit)
	{
		m_cMean = cFunc->GetParameter(1);
		m_cStdev = cFunc->GetParError(1);
	}
}

std::pair<double, double> TwoSidesGaussianFitter::getCombinedMeanStdev()
{
	std::vector<double> mean, weight;
	mean.push_back(m_lMean);
	mean.push_back(m_rMean);
	weight.push_back(m_lWeight);
	weight.push_back(m_rWeight);
	if (m_addCenterFit)
	{\
		mean.push_back(m_cMean);
		weight.push_back(m_cWeight);
	}
	return TwoSidesGaussianFitter::computeWeightedMeanAndStdev(mean, weight);
}

TwoSidesGaussianFitter::SmoothModeResult TwoSidesGaussianFitter::smoothModeBins(const TH1 & h, float outlierF, int startBin)
{
	SmoothModeResult result;
	// At very low stat (less entries than bins) potentially all bins are outliers.
	// --> adapt the outlier fraction.
	float occ = h.GetEffectiveEntries() / h.GetNbinsX();
	if ((occ < 1) && (h.GetEntries() < 0.5*h.GetNbinsX())) outlierF /= occ;

	// find max which is not an outlier.
	int maxbin = -1;

	float max = 0;
	float max_err = 0;
	for (int i = startBin; i <= h.GetNbinsX(); i++) {
		float c = h.GetBinContent(i);
		if (c > max) {
			float err = h.GetBinError(i);
			if ((err / c) < outlierF)
			{
				maxbin = i;
				max_err = err; max = c;
			}
		}
	}

	if (max == 0 || max_err == 0) return result;

	result.MaxBin = maxbin;

	// find all bins statistically compatible with maxbin
	for (int i = startBin; i <= h.GetNbinsX(); i++) {
		float c = h.GetBinContent(i);
		if (c == 0) continue;
		float err = h.GetBinError(i);
		if ((err / c) > outlierF) continue; // ignore outliers
		if ((max - c) < (max_err + err)) {
			result.ModeBins.push_back(i);
		}
	}

	return result;
}

std::pair<size_t, size_t> TwoSidesGaussianFitter::getNumberOfIgnoreSideBins(const TH1 & hist)
{
	size_t lNIgnoreBin = 0, rNIgnoreBin = 0;
	switch (m_distributionRangeMode)
	{
	case TwoSidesGaussianFitter::DistributionRangeMode::IgnoreEmptyBins:
	{
		//Left side empty bin
		for (Int_t binIndex = 1; binIndex <= hist.GetNbinsX(); binIndex++)
		{
			if (hist.GetBinContent(binIndex) <= 0)// && hist.GetBinError(binIndex) == 0)
			{
				lNIgnoreBin++;
			}
			else
			{
				break;
			}
		}
		//Right side empty bin
		for (Int_t binIndex = hist.GetNbinsX(); binIndex >= 1; binIndex--)
		{
			if (hist.GetBinContent(binIndex) <= 0)// && hist.GetBinError(binIndex) == 0)
			{
				rNIgnoreBin++;
			}
			else
			{
				break;
			}
		}
	}
	break;
	case TwoSidesGaussianFitter::DistributionRangeMode::IgnoreSideBinContent:
	{
		auto fullIntegral = hist.Integral(1, hist.GetNbinsX());
		//Left side bin
		for (Int_t binIndex = 1; binIndex <= hist.GetNbinsX(); binIndex++)
		{
			if (hist.Integral(1, binIndex) / fullIntegral < m_fractionOfSideBinContentToIgnore)
			{
				lNIgnoreBin++;
			}
			else
			{
				break;
			}
		}
		//Right side bin
		for (Int_t binIndex = hist.GetNbinsX(); binIndex >= 1; binIndex--)
		{
			if (hist.Integral(binIndex, hist.GetNbinsX()) / fullIntegral < m_fractionOfSideBinContentToIgnore)
			{
				rNIgnoreBin++;
			}
			else
			{
				break;
			}
		}
	}
	break;
	case TwoSidesGaussianFitter::DistributionRangeMode::All:
	default:
		break;
	}
	return std::make_pair(lNIgnoreBin, rNIgnoreBin);
}
